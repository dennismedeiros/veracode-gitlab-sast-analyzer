package main

import (
	"io"
	"os"
	"os/exec"
	"path/filepath"

	"github.com/urfave/cli"
)

const (
	pathOutput = "detailedreport.xml" // TODO
)

func analyzeFlags() []cli.Flag {
	return []cli.Flag{}
}

func analyze(c *cli.Context, path string) (io.ReadCloser, error) {
	var setupCmd = func(cmd *exec.Cmd) *exec.Cmd {
		cmd.Dir = path
		cmd.Env = os.Environ()
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		return cmd
	}

	err := setupCmd(exec.Command("java","-jar","../app/JavaWrapper.jar")).Run() // TODO
	//err := setupCmd(exec.Command("java -jar")).Run() // TODO
	if err != nil {
		return nil, err
	}

	return os.Open(filepath.Join(path, pathOutput))
}
