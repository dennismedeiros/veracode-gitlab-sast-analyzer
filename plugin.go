package main
//package plugin

import (
	"os"
	"path/filepath"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/plugin"
)

// Match is the stub for matching function that determines if the project
// matches a concrete analyzer plugin.
func Match(path string, info os.FileInfo) (bool, error) {
	// if info.Name() == "xxx" { // TODO
	// 	return true, nil
	// }
	switch filepath.Ext(info.Name()) {
	case ".jar", ".war", ".ear":
		return true, nil
	default:
		return false, nil
	}



	//return false, nil
}

func init() {
	plugin.Register("veracode-sast-platform", Match) // TODO
	//veracode-sast-pipeline
}
