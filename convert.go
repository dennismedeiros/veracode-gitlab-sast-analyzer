package main

import (
	"encoding/xml"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/issue"
)

const toolID = "XXX" // TODO

func convert(reader io.Reader, prependPath string) (*issue.Report, error) {
	var doc DetailedReportDocument

	err := xml.NewDecoder(reader).Decode(&doc)
	if err != nil {
		return nil, err
	}

	issues := extractSASTFlaws(&doc)

	report := issue.NewReport()
	report.Vulnerabilities = issues
	return &report, nil
}

// TODO Refactor into veracode content to get SAST results into normalized return
func extractSASTFlaws(detailedreport *DetailedReportDocument) []issue.Issue {

	var issues []issue.Issue
	
	var scanner issue.Scanner //TODO
	
	scanner.ID = "myscanner" //Possible Scan ID and Name?
	scanner.Name = "My Scanner"

	if detailedreport != nil {
		for i := 0; i < len(detailedreport.Severities); i++ {
			severity := detailedreport.Severities[i]
			for CategoriesItr:= 0; CategoriesItr < len(severity.Categories); CategoriesItr++ {
				category := severity.Categories[CategoriesItr]
				cwes := category.CWEs
				for CWEItr:= 0; CWEItr < len(cwes); CWEItr++ {
					cwe := cwes[CWEItr]
					if cwe.StaticFlaws.Flaws != nil {
						flaws := cwe.StaticFlaws.Flaws
						for flawsItr:= 0; flawsItr < len(flaws); flawsItr++ {
							//finds = append(finds, flaws[flawsItr])
							fnd :=  flaws[flawsItr]

							currentIssue :=
							issue.Issue {
								Category: issue.CategorySast,
								Name: cwe.Name,
								Message: cwe.Description,
								Description: fnd.Description,
								Severity: issue.SeverityLevelUnknown,
								Confidence: issue.ConfidenceLevelUnknown,
								Solution: "N/A",
								Scanner: scanner,
								Location: issue.Location {
									File: fnd.SourceFile,
									LineEnd: 0,
									Method: fnd.Type,
								},
							}

							issues = append(issues, currentIssue)
						}
					}
				}
			}
		}
	}
	return issues
}