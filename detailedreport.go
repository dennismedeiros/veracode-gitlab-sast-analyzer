package main
//package veracode //TODO

import (
	"encoding/xml"
)

// DetailedReportDocument will
type DetailedReportDocument struct {
	XMLName 	xml.Name 			`xml:"detailedreport"`
	AccountID 	int 				`xml:"account_id,attr"`
	AppID 		int 				`xml:"app_id,attr"`
	Name 		string 				`xml:"app_name,attr"`
	Severities []SeverityLevel 		`xml:"severity"`
}

// SeverityLevel will
type SeverityLevel struct {
	Level 		int 		  		`xml:"level,attr"`
	Categories 	[]WeaknessCategory 	`xml:"category"`
}

// WeaknessCategory will
type WeaknessCategory struct {
	ID 		int 		  	`xml:"categoryid,attr"`
	Name 	string 		  	`xml:"categoryname,attr"`
	CWEs 	[]CWECategory 	`xml:"cwe"`
}

// CWECategory will
type CWECategory struct {
	XMLName 	xml.Name 		`xml:"cwe"`
	ID 		int 		  		`xml:"cweid,attr"`
	Name 	string				`xml:"cwename,attr"`
	Description string  	    `xml:"description,attr"`
	StaticFlaws StaticFlaws 	`xml:"staticflaws"`
}

// StaticFlaws will
type StaticFlaws struct {
	XMLName 	xml.Name 	 		`xml:"staticflaws"`
	Flaws   	[]Flaw   	  		`xml:"flaw"`
}

// Flaw will
type Flaw struct {
	XMLName 			xml.Name	`xml:"flaw"`
	//Type    string   `xml:"type,attr"`
	//Name    string   `xml:"name"`
	IssueID 			int   		`xml:"issueid,attr"`
	Severity 			int			`xml:"severity,attr"`
	CategoryName 		string 		`xml:"categoryname,attr"`
	Module 				string 		`xml:"module,attr"`
	Type 				string 		`xml:"type,attr"`
	Description 		string 		`xml:"description,attr"`
	CWEID 				int   		`xml:"cweid,attr"`
	SourceFilePath		string		`xml:"sourcefilepath,attr"`
	SourceFile			string		`xml:"sourcefile,attr"`
	Line				string		`xml:"line,attr"`
	Scope				string		`xml:"scope,attr"`
	FunctionPrototype	string		`xml:"functionprototype,attr"`
}

func getSASTFindings(detailedreport *DetailedReportDocument)  {

}

//Normalize data into an array of arrays?
//func extractSASTFlaws(detailedreport *DetailedReportDocument) []string {

	//return nil
	// scanner.ID = "myscanner" //Possible Scan ID and Name?
	// scanner.Name = "My Scanner"

	// if detailedreport != nil {
	// 	for i := 0; i < len(detailedreport.Severities); i++ {
	// 		severity := detailedreport.Severities[i]
	// 		for CategoriesItr:= 0; CategoriesItr < len(severity.Categories); CategoriesItr++ {
	// 			category := severity.Categories[CategoriesItr]
	// 			cwes := category.CWEs
	// 			for CWEItr:= 0; CWEItr < len(cwes); CWEItr++ {
	// 				cwe := cwes[CWEItr]
	// 				if cwe.StaticFlaws.Flaws != nil {
	// 					flaws := cwe.StaticFlaws.Flaws
	// 					for flawsItr:= 0; flawsItr < len(flaws); flawsItr++ {
	// 						//finds = append(finds, flaws[flawsItr])
	// 						fnd :=  flaws[flawsItr]

	// 						currentIssue :=
	// 						issue.Issue {
	// 							Category: issue.CategorySast,
	// 							Name: cwe.Name,
	// 							Message: cwe.Description,
	// 							Description: fnd.Description,
	// 							Severity: issue.SeverityLevelUnknown,
	// 							Confidence: issue.ConfidenceLevelUnknown,
	// 							Solution: "N/A",
	// 							Scanner: scanner,
	// 							Location: issue.Location {
	// 								File: fnd.SourceFile,
	// 								LineEnd: 0,
	// 								Method: fnd.Type,
	// 							},
	// 						}

	// 						issues = append(issues, currentIssue)
	// 					}
	// 				}
	// 			}
	// 		}
	// 	}
	// }
	// return issues
//}

// func init() {
// 	fmt.Println("init DetailedReport.go")
// }
