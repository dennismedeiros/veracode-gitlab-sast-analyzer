package main

import (
	"os"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli"

	"gitlab.com/gitlab-org/security-products/analyzers/common/v2/command"

)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Usage = "Veracode Platform analyzer for GitLab SAST"
	app.Author = "Dennis Medeiros"

	app.Commands = command.NewCommands(command.Config{
		Match:        Match, // plugin.Match, 
		Analyze:      analyze,
		AnalyzeFlags: analyzeFlags(),
		Convert:      convert,
	})

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
